**Cadastrar produto, lançar compra de produto e/ou produto produzido, lançar venda de produto, inventário, relatorio de movimentação diária (produtos disponiveis a venda e quantos foram vendidos no dia)**

    Cadastro
        Produto
        Fornecedor
        Cliente - tem que ter por causa do fiado e reservas
        Despesas (fixas e variaveis - tais como luz, gas, telefone, impostos) - calcular a despesa do mes anterior e dividir pela quantidade de produtos vendido no mes anterior para prever o custo por produto no mes atual.
        Usuários (com perfil gestor, vendedor)
    Movimentação
        Entrada - Compra de Produto
        Reservas
        Venda
        Troca/devolução
        Lançamentos das perdas e dos produtos para uso-consumo (inventário);
    Relatórios
        Vendas a pagar (fiado)
        Vendas
        Listagem de Estoque / inventário (cod, descricao, qtda disponível, preço de compra, preço de venda, markup (margem de lucro), fornecedor
        Relatório de Estoque Mínimo
    Financeiro *
        Contas a pagar
        Registro de pagamento de fornecedores
        Registro de pagamento de salário
    Ferramentas
        Backup / Restore

**Listagem de estoque no Celular**


**Cadastro de produtos**
Produto = Nome (string 100), Descricao Completa (Nome + Marca + detalhe + conteudo), Descricao Reduzida (18 caracteres), Fornecedor, Marca, detalhe, conteudo (ex. quantidade da lata de milho = 350g)

**Tipos de produtos**

 - Matéria-prima - Material e/ou produto utilizado na produção. Não pode ser vendido.
 - Produto Processado - Sub-produto composto produzido utilizado na produção de um produto acabado ou produto processado. Não pode ser vendido.
 - Produto Acabado - Produto composto produzido e pronto para venda.
 - Produto Final - Produto comprado pronto para a venda.
 - Material Geral - Não é utilizado na produção e não pode ser vendido. Ex. Material de limpeza, Material de escritório, etc.

Exemplo: Risoto é um Produto Acabado. Milho verde é uma Matéria-prima. Caso queira vender milho verde, deve-se cadastrar um novo produto como Produto Final, ficando no sistema um milho verde como matéria-prima e outro como produto final.
Frango: Frango Resfriado é Matéria-prima, Frango Assado é Produto Acabado, Frango Desfiado, utilizado no risoto, é Produto Processado, Frango Assado Congelado, utilizado como frango desfiado no risoto, é Produto Processado.

*Produto composto: Lista de Materia-prima ou Produto Processado e Quantidade utilizado.*

Cálculo de Custos: Cada produto proderá ter incluido custos extra que seráo usado para calcular os custos finais do produto, tais como água, energia elétrica, mão-de-obra, taxas, embalagens, fretes, marketing, entre outros.


    Produto
    	Codigo
    	Descrição
    	Status
    	Custo Médio
    	Custo Total = Custo Médio x Quantidade
    	data inventário = tabela inventario
    	estoque minimo = 
    	item
    		Categoria / grupo / sub-grupo / familia
    		data de compra
    		conversão = converte unidade de compra para unidade padrão. ex uma lata de milho tem 350g
    		complemento / observação
    		fornecedor
    		Unidade de medida
    		Custo
    		Custo unitário = calculado convertendo o custo pela unidade padrão. Ex. Sendo unidade de medida gramas, uma lata de 350g que custa R$7,00, a grama custa R$0,02

Dados do Estoque
----------------
**Custo Médio**	
O calculo de custo médio é uma forma de valorização unitária, que corresponde a média ponderada entre os valores de movimentação do estoque.
Na prática, cada movimentação de entrada (entrada de nota, ajuste e inventário) modifica o custo médio, e cada saída, deve manter inalterado o custo médio, alterando somente o custo total ao movimentar o saldo em estoque.
O calculo de custo médio nas entradas depende diretamente do custo unitário, que é calculado nas entradas de nota, conforme configuração prévia.

O **custo unitário** pode ser calculado de três formas:

 - O custo unitário também pode ser definido manualmente via inventário
 - Fórmula de custo cadastrada pelo usuário nas variáveis para conta e vinculada a operação contábil de entrada;
 - Calculo padrão: (ValorTotalProduto + ValorIcmsSt + DespesasAcessorias – ImpostosCreditar)/QtdeItem;
 `	ValorTotalProduto: (Valor Unitario + IPI) * Qtde;
	ValorIcmsSt: Valor da substituição tributária (quando houver);
	DespesasAcessorias: Frete, seguro e despesas extras;
	ImpostosCreditar: São os impostos configurados na operação contábil para creditar. Ex: IPI, PIS, COFINS, etc;
	QtdeItem: Quantidade do item que será realizada a entrada;`

**Custo médio** = (CustoTotalAnterior + (QtdeMovimentacao*CustoUnitario)) / (SaldoAnterior + QtdeMovimentacao)
`	CustoTotalAnterior: Custo médio anterior * Saldo anterior do estoque;
	QtdeMovimentacao: Quantidade do item que será realizada a entrada;
	CustoUnitario: Custo unitário calculado;
	SaldoAnterior: Saldo anterior a movimentação de entrada;`

**Custo total** = CustoMedio * (SaldoAnterior + QtdeMovimentacao)
`	CustoMedio: Conforme fórmula anterior;
	SaldoAnterior: Saldo anterior a movimentação de entrada;
	QtdeMovimentacao: Quantidade do item que será realizada a entrada;`


As unidades de medida são agrupadas em grandezas, e a cada grandeza é fixada uma "unidade padrão" que estabelece o fator de conversão para as demais.

    Area
        m2 - Metro quadrado (padrão)
        cm2 - Centímetro quadrado (0,0001) 
    Comprimento
        m - Metro (padrão)
        cm - Centímetro (0,01)
    Embalagem
        UN - Unidade (padrão)
        CT - Cartela (1)
        CX - Caixa (1)
        DZ - Duzia (12)
        GZ - Groza (144)
        PA - Par (2)
        PÇ - Peça (1)
        PR - Par (2)
        PT - Pacote (1)
        RL - Rolo (1) 
    Peso
        kg - Kilograma (padrão)
        g - Grama (0,001)
        SC60 - Saca 60Kg (60) 
    Volume
        l - Litro (padrão)
        m3 - Metro cúbico (1000)
        ml - Mililitro (0,001) 
    Erro
        X - Nao definida (padrão)

**Bobina ECF padrão 80mm** - 48 colunas - 80mm / 76mm / 57mm / 89mm
Para que o item possa ser impresso em uma única linha, o seu código e sua descrição (juntos) não podem ultrapassar 16 caracteres; a quantidade deve possuir até 2 dígitos (entre 1 e 99) e o valor unitário deve possuir até 3 dígitos inteiros (entre 0,01 e 999,99)

USAR COMO PADRÃO 40 COLUNAS - sendo Descricao(18) Valor(7) Qtda(6) Subtotal(9)

Codigo: STRING até 13 caracteres
Descricao: STRING até 29
Quantidade: STRING com 7, sendo 4 a parte inteira e 3 as casas decimais - deixar 6 sendo 2 parte inteira e 3 parte fracionada
Valor(string 13, já com virgula e duas decimais) (bematech 8 parte inteira e 3 parte fracionada) - deixar 9 com mascara, salvar sem mascara 99.999,99
desconto (8,2 para valor, e 4 para %)
CPF/CNPJ (string 14 - sem formatação)
uf (2)
cpf (08)
data (dd/mm/aaaa)
FormaPagamento string 16

**Leitura X** - Indica as vendas e a totalização que a impressora efetuou até o momento. 	
**Redução Z** - Fecha o caixa e trava a impressora. Exibe todas as vendas, cancelamentos, descontos , etc e fecha o caixa. Deve ser usada para encerrar o dia . Depois de efetuada você só vai poder realizar operação no dia seguinte.

Gestão de estoque
-----------------
**DADOS DO ITEM** Indicar a descrição do produto / material e seu respectivo código de cadastro estabelecido pela empresa. Esta uniformização fortalece o controle e permite que os registros sejam efetuados com um padrão previamente definido.

**DESCRIÇÃO DA UNIDADE** Informar a unidade referente ao item controlado. Este registro deve levar em consideração a característica de cada material e sua identificação de medida tais como quilograma, metro, peça.
É recomendável que se padronize este registro visando evitar divergências que podem influenciar no resultado final do estoque.

**QUANTIDADE EM ESTOQUE** Efetuar a informação do estoque mínimo e máximo, assim como o estoque físico atual. As definições de estoques mínimos e máximos devem ser realizadas por um responsável pela previsão de estoques, que considera os níveis de produção, consumo e reposição, para que possa estabelecer estes níveis com maior segurança.
O recomendável é que o cálculo do saldo em estoque seja realizado automaticamente e ser checado para evitar erros.

**DATA DE MOVIMENTAÇÃO** Efetuar o registro das datas efetivas de entrada e saída dos itens, utilizando as datas constantes dos respectivos documentos fiscais ou de movimentação interna (caso de aplicações em produção ou transferências / ajustes).

**QUANTIDADE** Registrar as quantidades de entradas e saídas conforme as informações dos respectivos documentos fiscais ou de movimentação interna (caso de aplicações em produção ou transferências / ajustes).

**CUSTOS** Proceder ao registro dos custos totais e unitários de entrada, saída e do estoque atualizado.

Deve ser ressaltado que este instrumento de controle deve ser realizado por pessoal independente daquele que é responsável pelas compras e pagamentos da empresa, de preferência sendo exercido por pessoa diferente daquela que efetua as movimentações físicas de estoque (recebimento, fornecimento de materiais e produtos), sendo sua responsabilidade atestar a efetiva movimentação de estoques e suas existências físicas.

