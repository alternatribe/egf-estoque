package net.endrigo.EGF_Estoque.Business;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.endrigo.EGF_Estoque.App;
import net.endrigo.EGF_Estoque.Domain.Produto;
import net.endrigo.EGF_Estoque.Domain.ProdutoCompra;
import net.endrigo.EGF_Estoque.Persistence.ProdutoDAO;

public class CadastroProdutoBC {
	static Logger logger = LoggerFactory.getLogger(App.class);

	public List<Produto> localizarProdutos(String... tipos) {
		logger.debug("procurando produtos " + tipos);
		ProdutoDAO produtoDao = new ProdutoDAO();
		List<Produto> produtos = produtoDao.findAll(tipos);
		return produtos;
	}

	public List<Produto> localizarTodosProdutosVenda() {
		return this.localizarProdutos("ProdutoFinal", "ProdutoFinalizado");
	}

	public List<Produto> localizarTodosProdutosFinalizados() {
		return this.localizarProdutos("ProdutoFinalizado");
	}

	public Produto find(String nome) {
		logger.debug("procurando produto");
		ProdutoDAO dao = new ProdutoDAO();
		Produto p = dao.find(nome);
		return p;
	}

	public void inserirProduto(Produto produto) {
		logger.debug("inserindo produto: " + produto);
		ProdutoDAO produtoDao = new ProdutoDAO();
		produtoDao.save(produto);
	}

	public void atualizarProduto(Produto produto) {
		logger.debug("atualizando produto: " + produto);
		ProdutoDAO produtoDao = new ProdutoDAO();
		produtoDao.update(produto);
	}

	public void limparTodosProdutos() {
		logger.debug("limpando todos os produtos");
		ProdutoDAO produtoDao = new ProdutoDAO();
		produtoDao.removeAll();
	}

	public void excluirProduto(Produto produto) {
		logger.debug("deletando produto: " + produto);
		ProdutoDAO dao = new ProdutoDAO();
		dao.remove(produto);
	}

	public void atualizarCompra(ProdutoCompra produto, Long quantidade, String valor) {
		if (produto instanceof ProdutoCompra) {
			logger.debug("atualizando produto comprado");
			// produto.se produto.getQuantidadeDisponivel()
			(produto).atualizarProdutoComprado(quantidade, valor);
			this.atualizarProduto((Produto) produto);
		} else {
			logger.debug("produto não pode ser comprado.");
		}

	}

	public void simular(String produto, final String ingrediente, double custo) {
		// ProdutoFinalizado p = (ProdutoFinalizado) find(produto);
		// for (Ingrediente i : p.getIngredientes()) {
		// if (i.getProduto().getNome().equals(ingrediente)) {
		// i.getProduto()
		// }
		// }

	}

}
