package net.endrigo.EGF_Estoque.Business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.endrigo.EGF_Estoque.App;
import net.endrigo.EGF_Estoque.Domain.ItemCompra;
import net.endrigo.EGF_Estoque.Persistence.ItemCompraDAO;

public class CadastrarCompraBC {
	static Logger logger = LoggerFactory.getLogger(App.class);
	
	public void lancarCompra(ItemCompra item) {
        logger.debug("lançamento de compra: " + item);
	    ItemCompraDAO dao = new ItemCompraDAO();
	    CadastroProdutoBC produtoBC = new CadastroProdutoBC();
	    produtoBC.atualizarCompra(item.getProduto(), item.getQuantidadeComprada(), item.getValorUnitario().toString());
	    dao.save(item);
	}

}
