package net.endrigo.EGF_Estoque.Converter;

import java.math.BigDecimal;

/**
 * Exemplo de Conversor de unidade de medida<br>
 * 
 * O primeiro exemplo usa constantes<br>
 * o segundo exemplo usa enum (achei melhor)<br><br>
 * 
 * demais medidas, como <b>lata de milho 355g</b> seria convertido para uma dessas unidades básicas<br>
 * essas demais medidas devem ser cadastradas em banco
 */
public class UnidadeMedidaConverter {
	public static final Integer UNIDADE_G = 1;
	// Considerando como menor unidade ou seja todas as unidades trabalharam com ela.
	public static final Integer UNIDADE_KG = UNIDADE_G * 1000;
	public static final Integer UNIDADE_SACO_CIMENTO = UNIDADE_KG * 100;
	
	
	public enum Massa {
		TONELADA(1D), QUILOGRAMA(1000D), HECTOGRAMA(10000D), GRAMA(1000000D), CENTIGRAMA(100000000D), 
		QUILATE(5000000D), MILIGRAMA(1000000000D), MICROGRAMA(1000000000000D), NANOGRAMA(1000000000000000D),
		ONCA_TROY(32150.7453282234D), ONCA_AVORDUPIOS(35273.9619495804D), LIBRA_TROY(2679.228880719D),
		LIBRA_AVORDUPOIS(2204.62262184878D), LIBRA_IMPERIAL(2204.62277738034D), ARROBA(35.2739619495804D),
		ARROBA_PT_BR(68.0781537204711D), ARROBA_PT_BR_APROX(66.6666666666667D), ARROBA_SPA(61.2395172735771D);
		private BigDecimal unidade;
		private Massa(double unidade) {
			this.unidade = new BigDecimal(unidade);
		}
		@Override
		public String toString() {
			return String.valueOf(unidade);
		}
	}
	
	public enum Volume {
		QUILOLITRO(1D), HECTOLITRO(10D), DECALITRO(100D), LITRO(1000D), DECILITRO(10000D),
		CENTILITRO(100000D), MILILITRO(1000000D);
		private BigDecimal unidade;
		private Volume(double unidade) {
			this.unidade = new BigDecimal(unidade);
		}
		@Override
		public String toString() {
			return String.valueOf(unidade);
		}
	}
	
	public enum Comprimento {
		KM(1D), HECTOMETRO(10D), DECAMETRO(100D), METRO(1000D), DECIMETRO(10000D),
		CENTIMETRO(100000D), MILIMETRO(1000000D), MICROMETRO(1000000000D),
		NANOMETRO(1000000000000D), ANGSTROM(10000000000000D),
		POLEGADA(39370.07874015748031496062992126D);
		private BigDecimal unidade;
		private Comprimento(double unidade) {
			this.unidade = new BigDecimal(unidade);
		}
		@Override
		public String toString() {
			return String.valueOf(unidade);
		}
	}

	public static BigDecimal converter(Integer qtd, Integer unidadeAtual, Integer unidadeConvertida) {
		return new BigDecimal((qtd * unidadeAtual) / unidadeConvertida);
	}
	
	public static BigDecimal converter(String valor, Massa unidadeEntrada, Massa unidadeSaida) {
		BigDecimal val = new BigDecimal(valor);
		if (unidadeEntrada.unidade.compareTo(unidadeSaida.unidade) == 1)
			return (val.divide((unidadeEntrada.unidade.divide(unidadeSaida.unidade))));
		else return (val.multiply((unidadeSaida.unidade.divide(unidadeEntrada.unidade))));
	}
	
	public static BigDecimal converter(String valor, Volume unidadeEntrada, Volume unidadeSaida) {
		BigDecimal val = new BigDecimal(valor);
		if (unidadeEntrada.unidade.compareTo(unidadeSaida.unidade) == 1)
			return (val.divide((unidadeEntrada.unidade.divide(unidadeSaida.unidade))));
		else return (val.multiply((unidadeSaida.unidade.divide(unidadeEntrada.unidade))));
	}

	public static void main(String[] args) {
		BigDecimal convertido = UnidadeMedidaConverter.converter(200, UnidadeMedidaConverter.UNIDADE_KG,
																  UnidadeMedidaConverter.UNIDADE_SACO_CIMENTO);
		System.out.println("Valor convertido = " + convertido); 

		BigDecimal convertido2 = UnidadeMedidaConverter.converter(2, UnidadeMedidaConverter.UNIDADE_SACO_CIMENTO,
																   UnidadeMedidaConverter.UNIDADE_KG);
		System.out.println("Valor convertido = " + convertido2); 
		
		System.out.println(converter("2", Massa.QUILOGRAMA, Massa.GRAMA));
		System.out.println(converter("2000", Massa.GRAMA, Massa.QUILOGRAMA));
		System.out.println(converter("2", Volume.LITRO, Volume.MILILITRO));
		System.out.println(converter("2000", Volume.MILILITRO, Volume.LITRO));	
	}
}
