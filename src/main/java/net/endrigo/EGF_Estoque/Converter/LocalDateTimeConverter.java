package net.endrigo.EGF_Estoque.Converter;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class LocalDateTimeConverter implements AttributeConverter<LocalDateTime, Long> {

	@Override
	public Long convertToDatabaseColumn(LocalDateTime localDateTime) {
		return (localDateTime == null ? null : Timestamp.valueOf(localDateTime).getTime());
	}

	@Override
	public LocalDateTime convertToEntityAttribute(Long time) {
		if (time == null) {
			return null;
		}
		Timestamp timestamp = new Timestamp(time);
		return timestamp.toLocalDateTime();
	}


}
