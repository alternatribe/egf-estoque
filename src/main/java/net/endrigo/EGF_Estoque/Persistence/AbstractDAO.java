package net.endrigo.EGF_Estoque.Persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaQuery;

import net.endrigo.EGF_Estoque.Domain.AbstractBean;
import net.endrigo.EGF_Estoque.Domain.Produto;

public abstract class AbstractDAO<T extends AbstractBean<?>, PK extends Serializable> extends BaseDAO {
    private Class<T> entityClass;

    public AbstractDAO(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    /**
     * Saves an entity.
     * 
     * @param entity
     */
    public void save(T entity) {
        if (entity != null) {
            try {
                this.beginTransaction();
                getEntityManager().persist(entity);
                getEntityManager().flush();
                this.commit();
            } catch (Exception e) {
                this.rollback();
                // Abafado a exception, vai somente exibir mensagem no log
                if (entity instanceof Produto) {
                    if (e.getMessage().contains("[SQLITE_CONSTRAINT_UNIQUE]")) {
                        logger.debug(entity.getClass().getSimpleName() +  " já cadastrado.");
                    }
                    if (e.getMessage().contains("[SQLITE_CONSTRAINT_NOTNULL]")) {
                        logger.debug(entity.getClass().getSimpleName() + " não cadastrado (informação incompleta).");
                    }
                }
            } finally {
                this.closeTransaction();
            }

        }
    }

    /**
     * Merges objects with the same identifier within a session into a newly created object.
     * 
     * @param entity
     * @return a newly created instance merged.
     */
    public T update(T entity) {
        T entityNew = null;
        if (entity != null) {
            try {
                this.beginTransaction();
                entityNew = getEntityManager().merge(entity);
                this.commit();
            } catch (Exception e) {
                this.rollback();
                // Abafado a exception, vai somente exibir mensagem no log
                if (entity instanceof Produto) {
                    if (e.getMessage().contains("[SQLITE_CONSTRAINT_UNIQUE]")) {
                        logger.debug(entity.getClass().getSimpleName() +  " já cadastrado.");
                    }
                    if (e.getMessage().contains("[SQLITE_CONSTRAINT_NOTNULL]")) {
                        logger.debug(entity.getClass().getSimpleName() + " não cadastrado (informação incompleta).");
                    }
                }
            } finally {
                this.closeTransaction();
            }
        }
        return entityNew;
    }

    /**
     * Delete the entity by id.
     * 
     * @param entityID
     */
    public void delete(PK entityID) {
        if (entityID != null) {
            try {
                this.beginTransaction();
                T entityToBeRemoved = getEntityManager().getReference(entityClass, entityID);
                getEntityManager().remove(entityToBeRemoved);
                this.commit();
            } catch (Exception e) {
                this.rollback();
                logger.info("Não foi possível apagar " + entityClass.getSimpleName());
            } finally {
                this.closeTransaction();
            }
        }
    }

    /**
     * Delete the entity
     */
    public void remove(T entity) {
        if (entity != null) {
            try {
                this.beginTransaction();
                T entityToBeRemoved = getEntityManager().getReference(entityClass, entity.getId());
                getEntityManager().remove(entityToBeRemoved);
                this.commit();
            } catch (Exception e) {
                this.rollback();
                logger.info("Não foi possível apagar " + entity.getClass().getSimpleName());
            } finally {
                this.closeTransaction();
            }
        }
    }

    public void removeAll() {
        try {
            this.beginTransaction();
            getEntityManager().createQuery("DELETE FROM " + entityClass.getSimpleName()).executeUpdate();
            this.commit();
        } catch (Exception e) {
            this.rollback();
            logger.info("Não foi possível apagar os registros de " + entityClass.getSimpleName());
        } finally {
            this.closeTransaction();
        }
    }

    /**
     * Find entity by id
     * 
     * @param entityID
     * @return entity
     */
    public T find(PK entityID) {
        try {
            return getEntityManager().find(entityClass, entityID);
        } catch (NoResultException e) {
            return null;
        }

    }

    /**
     * Find entity by id and return reference
     * 
     * @param entityID
     * @return entity reference
     */
    public T findReferenceOnly(PK entityID) {
        try {
            return getEntityManager().getReference(entityClass, entityID);
        } catch (NoResultException e) {
            return null;
        }

    }

    /**
     * Find all entity
     * 
     * @return List of entity
     */
    public List<T> findAll() {
        CriteriaQuery<T> cq = getEntityManager().getCriteriaBuilder().createQuery(entityClass);
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }
}
