package net.endrigo.EGF_Estoque.Persistence;

import net.endrigo.EGF_Estoque.Domain.Fornecedor;

public class FornecedorDAO extends AbstractDAO<Fornecedor, Integer> {

	public FornecedorDAO() {
		super(Fornecedor.class);
	}

}
