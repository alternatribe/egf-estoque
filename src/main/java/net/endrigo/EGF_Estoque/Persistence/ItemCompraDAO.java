package net.endrigo.EGF_Estoque.Persistence;

import net.endrigo.EGF_Estoque.Domain.ItemCompra;

public class ItemCompraDAO extends AbstractDAO<ItemCompra, Integer> {

	public ItemCompraDAO() {
		super(ItemCompra.class);
	}
}
