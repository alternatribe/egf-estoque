package net.endrigo.EGF_Estoque.Persistence;

import java.util.Arrays;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import net.endrigo.EGF_Estoque.Domain.Produto;

public class ProdutoDAO extends AbstractDAO<Produto, Integer> {

	public ProdutoDAO() {
		super(Produto.class);
	}

	public List<Produto> findAllbyTipo() {
		String jpql = "select p from Produto p order by p.tipo ASC, p.nome ASC";
		// String jpql = "select p from Produto p order by TYPE(p) ASC, p.nome ASC";
		TypedQuery<Produto> query = getEntityManager().createQuery(jpql, Produto.class);
		return query.getResultList();
	}

	public List<Produto> findAllbyName() {
		String jpql = "select p from Produto p order by p.nome ASC";
		TypedQuery<Produto> query = getEntityManager().createQuery(jpql, Produto.class);
		return query.getResultList();
	}

	public List<Produto> findAll(String... tiposProduto) {
		if (tiposProduto.length == 0) {
			return findAll();
		} else {
			String jpql = "select p from Produto p where p.tipo in :tiposProduto order by p.tipo ASC, p.nome asc ";
			TypedQuery<Produto> query = getEntityManager().createQuery(jpql, Produto.class);
			query.setParameter("tiposProduto", Arrays.asList(tiposProduto));
			return query.getResultList();
		}
	}

	public Produto find(String nome) {
		try {
			String jpql = "select p from Produto p where p.nome = :nome";
			TypedQuery<Produto> query = getEntityManager().createQuery(jpql, Produto.class);
			query.setParameter("nome", nome);
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

}
