package net.endrigo.EGF_Estoque.Persistence;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.endrigo.EGF_Estoque.Util.JpaUtil;

public abstract class BaseDAO {

    public Logger logger = LoggerFactory.getLogger(this.getClass());

    private EntityManager entityManager;

    protected EntityManager getEntityManager() {
        if (entityManager == null) {
            entityManager = JpaUtil.getEntityManager();
        }
        return entityManager;
    }

    public void beginTransaction() {
        if (entityManager == null) {
            entityManager = JpaUtil.getEntityManager();
        }
        entityManager.getTransaction().begin();
    }

    public void commit() {
        entityManager.getTransaction().commit();
    }

    public void rollback() {
        if (entityManager.isOpen() && entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().rollback();
        }
    }

    public void closeTransaction() {
        JpaUtil.closeEntityManager();
    }

    public void flush() {
        entityManager.flush();
    }

    public void joinTransaction() {
        entityManager = JpaUtil.getEntityManager();
        entityManager.joinTransaction();
    }

}
