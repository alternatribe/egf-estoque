package net.endrigo.EGF_Estoque.Util;

public enum Order {
	ASC, DESC;

	/**
	 * <p>
	 * The ordering of a collection of objects.
	 * </p>
	 * 
	 * <ul>
	 * <li><b>ASC</b>: ascending order.
	 * <li><b>DESC</b>: descending order.
	 * </ul>
	 */
	public boolean isAscOrder() {
		return ASC.equals(this);
	}
}
