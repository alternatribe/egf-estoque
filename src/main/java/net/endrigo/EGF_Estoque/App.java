package net.endrigo.EGF_Estoque;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.endrigo.EGF_Estoque.Business.CadastroProdutoBC;
import net.endrigo.EGF_Estoque.Constant.EnumSituacaoProduto;
import net.endrigo.EGF_Estoque.Constant.EnumUnidadeMedida;
import net.endrigo.EGF_Estoque.Domain.Custo;
import net.endrigo.EGF_Estoque.Domain.Ingrediente;
import net.endrigo.EGF_Estoque.Domain.Insumo;
import net.endrigo.EGF_Estoque.Domain.MateriaPrima;
import net.endrigo.EGF_Estoque.Domain.Produto;
import net.endrigo.EGF_Estoque.Domain.ProdutoFinal;
import net.endrigo.EGF_Estoque.Domain.ProdutoFinalizado;
import net.endrigo.EGF_Estoque.Domain.ProdutoProcessado;
import net.endrigo.EGF_Estoque.Domain.ProdutoVenda;

/**
 * EGF_Estoque 
 */
public class App {
    static Logger logger = LoggerFactory.getLogger(App.class);

    static CadastroProdutoBC produtoBC = new CadastroProdutoBC();

    public static void main(String[] args) throws Exception {
        try {
            logger.info("EGF_Estoque Starting...");

            apagarBaseInicial();
            if (produtoBC.localizarProdutos().isEmpty()) {
                cadastrarBaseInicial();
            }

            listarProdutos("ProdutoFinalizado");
//            listarProdutosVendaAtivos();
            listarProdutosVendaInativos();
//            listarProdutos();
            
//            // atualizar quantidade e preço
//            Produto p = bc.find("Sprite 2L");
//            if (p != null && p instanceof ProdutoCompra) {
//                System.out.println(p);
//                CadastrarCompraBC compra = new CadastrarCompraBC();
//                ItemCompra item = new ItemCompra((ProdutoCompra) p, 10L, "4");
//                compra.lancarCompra(item);
//                System.out.println(p);
//                item = new ItemCompra((ProdutoCompra) p, 5L, "3");
//                compra.lancarCompra(item);
////                p.setEstoqueMinimo(1L);
////                bc.atualizarProduto(p);
////                bc.excluirProduto(p);
//                System.out.println(p);
//            }
//            bc.listarProdutosVenda();
//                bc.simular("Frango Assado", "Frango", 4.6);

        } catch (Exception e) {
            logger.error("", e);
        } finally {
            logger.info("<<EGF_Estoque finalizado.>>");
        }
    }
    @SuppressWarnings("unused")
	private static void apagarBaseInicial() {
    	produtoBC.limparTodosProdutos();
    }
    
    @SuppressWarnings("unused")
    private static void cadastrarBaseInicial() {

        ArrayList<Ingrediente> ingredientes = null;
        ArrayList<Custo> custos = null;


        // sacola é custo indireto ?
        // gas é custo indireto ?
        // carvao custo indireto ?
        // papel acoplado custo indireto ?

        // Insumo
        Insumo celofane = new Insumo("Filme Poliéster / Celofane", EnumUnidadeMedida.KG, "35", true);
        produtoBC.inserirProduto(celofane);
        Insumo embalagemFarofa = new Insumo("Pote 200ml p/ Farofa", EnumUnidadeMedida.UNI, "0.45", true);
        produtoBC.inserirProduto(embalagemFarofa);
        Insumo embalagemFarofaTampa = new Insumo("Tampa do Pote 200ml p/ Farofa", EnumUnidadeMedida.UNI, "0.40", true);
        produtoBC.inserirProduto(embalagemFarofaTampa);
        Insumo embalagemRisoto = new Insumo("Marmita Isopor 750ml p/ Risoto", EnumUnidadeMedida.UNI, "0.40", true);
        produtoBC.inserirProduto(embalagemRisoto);
        Insumo embalagemMaionese = new Insumo("Pote Descartável para Maionese 500ml", EnumUnidadeMedida.UNI, "0.40", true);
        produtoBC.inserirProduto(embalagemMaionese);
        Insumo papelAcoplado = new Insumo("Papel Acoplado", EnumUnidadeMedida.UNI, "0.22", true);
        produtoBC.inserirProduto(papelAcoplado);
        Insumo sacola = new Insumo("Sacola", EnumUnidadeMedida.UNI, "0.040", true);
        produtoBC.inserirProduto(sacola);
        Insumo barbante = new Insumo("Barbante Rolo Algodão N°8 200 Gramas (preço por metro)", EnumUnidadeMedida.UNI, "0.5", true);
        produtoBC.inserirProduto(barbante);
        Insumo papelAluminio = new Insumo("Papel Aluminio", EnumUnidadeMedida.UNI, "4.40", true);
        produtoBC.inserirProduto(papelAluminio);
//        Insumo gas = new Insumo("Gas", EnumUnidadeMedida.UNI, "65", true);
//        gas.setDescricao("Soma-se a quantidade de todos os produtos estimado de venda por unidade. "
//        		+ "Atualmente na máquina é estimado 4 gas por mes, para 96 frangos, 16kg de coxa, 16kg de pernil, 8kg de copa lombo e 8kg de linguiça. "
//        		+ "(96 + 16 + 20 + 8 + 8 ) / 4 gas = 37");
//        gas.setQuantidadeItensPorInsumo(37L);
//        bc.inserirProduto(gas);
//        Insumo luz = new Insumo("Luz", EnumUnidadeMedida.UNI, "100", true);
//        produtoBC.inserirProduto(luz);
        Insumo carvao = new Insumo("Carvão", EnumUnidadeMedida.KG, "2", true);
        produtoBC.inserirProduto(carvao);

        
        // Matéria Prima
        MateriaPrima frango = new MateriaPrima("Frango Resfriado/Congelado", EnumUnidadeMedida.KG, "4.65", true);
        MateriaPrima frangoCarcaca = new MateriaPrima("Frango Resfriado Carcaça", EnumUnidadeMedida.KG, "5.40", true);
        MateriaPrima costela = new MateriaPrima("Costela Bovina Resfriada", EnumUnidadeMedida.KG, "13.98", true);
        MateriaPrima pernil = new MateriaPrima("Pernil Suino Resfriado", EnumUnidadeMedida.KG, "8.99", true);
        MateriaPrima copa = new MateriaPrima("Copa Lombo Resfriado", EnumUnidadeMedida.KG, "13.5", true);
        MateriaPrima coxa = new MateriaPrima("Coxa com Sobrecoxa Resfriado", EnumUnidadeMedida.KG, "4.59", true);
        MateriaPrima linguica = new MateriaPrima("Linguiça Toscana Frimesa", EnumUnidadeMedida.KG, "10.98", true);

        MateriaPrima oleo = new MateriaPrima("Óleo de soja", EnumUnidadeMedida.L, "3.61", true);
        MateriaPrima colorau = new MateriaPrima("Colorífico", EnumUnidadeMedida.KG, "4", true);
        MateriaPrima vinagre = new MateriaPrima("Vinagre", EnumUnidadeMedida.L, "1.45", true);
        MateriaPrima vinho = new MateriaPrima("Vinho Branco", EnumUnidadeMedida.L, "7.8", true);
        MateriaPrima cenoura = new MateriaPrima("Cenoura", EnumUnidadeMedida.KG, "1.79", true);
        MateriaPrima cebola = new MateriaPrima("Cebola", EnumUnidadeMedida.KG, "1.68", true);
        MateriaPrima salsa = new MateriaPrima("Salsa Desidratada", EnumUnidadeMedida.KG, "35", true);
        MateriaPrima salvia = new MateriaPrima("Salvia Desidratada", EnumUnidadeMedida.KG, "41", true);
        MateriaPrima tomilho = new MateriaPrima("Tomilho Desidratado", EnumUnidadeMedida.KG, "29", true);
        MateriaPrima louro = new MateriaPrima("Louro Desidratado", EnumUnidadeMedida.KG, "26", true);
        MateriaPrima sal = new MateriaPrima("Sal", EnumUnidadeMedida.KG, "1.29", true);

        MateriaPrima manjerona = new MateriaPrima("Manjerona Desidratado", EnumUnidadeMedida.KG, "23", true);
        MateriaPrima manjericao = new MateriaPrima("Manjericao Desidratado", EnumUnidadeMedida.KG, "14", true);
        MateriaPrima salGrosso = new MateriaPrima("Sal Grosso", EnumUnidadeMedida.KG, "1.69", true);

        MateriaPrima limao = new MateriaPrima("Limão", EnumUnidadeMedida.KG, "7.99", true);
        MateriaPrima acucar = new MateriaPrima("Açucar Refinado", EnumUnidadeMedida.KG, "1.99", true);

        MateriaPrima batata = new MateriaPrima("Batata Monalisa", EnumUnidadeMedida.KG, "2.29", true);
        MateriaPrima leite = new MateriaPrima("Leite", EnumUnidadeMedida.L, "2.2", true);
        MateriaPrima milho = new MateriaPrima("Milho Verde", EnumUnidadeMedida.UNI, "1.49", true);
        MateriaPrima cheiroVerde = new MateriaPrima("Cheiro Verde", EnumUnidadeMedida.UNI, "0.85", true);
        MateriaPrima ovo = new MateriaPrima("Ovo", EnumUnidadeMedida.UNI, "0.33", true);
        MateriaPrima maionese = new MateriaPrima("Maionese (Molho)", EnumUnidadeMedida.KG, "11.4", true);

        MateriaPrima arroz = new MateriaPrima("Arroz", EnumUnidadeMedida.KG, "1.8", true);
        MateriaPrima tomate = new MateriaPrima("Tomate", EnumUnidadeMedida.KG, "3.19", true);
        MateriaPrima polpa = new MateriaPrima("Polpa de Tomate", EnumUnidadeMedida.UNI, "2.5", true);

        MateriaPrima bacon = new MateriaPrima("Bacon", EnumUnidadeMedida.KG, "13.98", true);
        MateriaPrima farinhaMandioca = new MateriaPrima("Farinha de Mandioca", EnumUnidadeMedida.KG, "4.29", true);
        MateriaPrima farinhaMilhoBiju = new MateriaPrima("Farinha Biju de Milho Amarela", EnumUnidadeMedida.KG, "3.6", true);
        MateriaPrima azeitona = new MateriaPrima("Azeitona", EnumUnidadeMedida.KG, "38", true);
        MateriaPrima moela = new MateriaPrima("Moela", EnumUnidadeMedida.KG, "5", true);
        MateriaPrima figado = new MateriaPrima("Fígado de Galinha", EnumUnidadeMedida.KG, "4", true);

        produtoBC.inserirProduto(frango);
        produtoBC.inserirProduto(frangoCarcaca);
        produtoBC.inserirProduto(costela);
        produtoBC.inserirProduto(pernil);
        produtoBC.inserirProduto(copa);
        produtoBC.inserirProduto(coxa);
        produtoBC.inserirProduto(linguica);
        produtoBC.inserirProduto(oleo);
        produtoBC.inserirProduto(colorau);
        produtoBC.inserirProduto(vinagre);
        produtoBC.inserirProduto(vinho);
        produtoBC.inserirProduto(cenoura);
        produtoBC.inserirProduto(cebola);
        produtoBC.inserirProduto(salsa);
        produtoBC.inserirProduto(salvia);
        produtoBC.inserirProduto(tomilho);
        produtoBC.inserirProduto(louro);
        produtoBC.inserirProduto(sal);
        produtoBC.inserirProduto(manjerona);
        produtoBC.inserirProduto(manjericao);
        produtoBC.inserirProduto(salGrosso);
        produtoBC.inserirProduto(limao);
        produtoBC.inserirProduto(acucar);
        produtoBC.inserirProduto(batata);
        produtoBC.inserirProduto(leite);
        produtoBC.inserirProduto(milho);
        produtoBC.inserirProduto(cheiroVerde);
        produtoBC.inserirProduto(ovo);
        produtoBC.inserirProduto(maionese);
        produtoBC.inserirProduto(arroz);
        produtoBC.inserirProduto(tomate);
        produtoBC.inserirProduto(polpa);
        produtoBC.inserirProduto(bacon);
        produtoBC.inserirProduto(farinhaMandioca);
        produtoBC.inserirProduto(farinhaMilhoBiju);
        produtoBC.inserirProduto(azeitona);
        produtoBC.inserirProduto(moela);
        produtoBC.inserirProduto(figado);


        // Produto Final
        produtoBC.inserirProduto(new ProdutoFinal("Coca-Cola 2L", EnumUnidadeMedida.UNI, "5.99", "7.5", true));
        produtoBC.inserirProduto(new ProdutoFinal("Fanta Laranja 2L", EnumUnidadeMedida.UNI, "5.39", "6", true));
        produtoBC.inserirProduto(new ProdutoFinal("Guaraná Kuat 2L", EnumUnidadeMedida.UNI, "2.99", "6", true));
        produtoBC.inserirProduto(new ProdutoFinal("Sprite 2L", EnumUnidadeMedida.UNI, "4.99", "6", true));
        produtoBC.inserirProduto(new ProdutoFinal("Coca-Cola Zero 2L", EnumUnidadeMedida.UNI, "5.99", "7.5", true));
        produtoBC.inserirProduto(new ProdutoFinal("Fanta Laranja Zero 2L", EnumUnidadeMedida.UNI, "3.49", "6", true));
        produtoBC.inserirProduto(new ProdutoFinal("Guaraná Kuat Zero 2L", EnumUnidadeMedida.UNI, "2.99", "6", true));
        produtoBC.inserirProduto(new ProdutoFinal("Sprite Zero 2L", EnumUnidadeMedida.UNI, "5.39", "6", true));
        
        // Produto Processado
        ProdutoProcessado temperoFrango = new ProdutoProcessado("Tempero para um kg de frango");
        ingredientes = new ArrayList<Ingrediente>();
        ingredientes.add(new Ingrediente(oleo, "0.020"));
        ingredientes.add(new Ingrediente(colorau, "0.004"));
        ingredientes.add(new Ingrediente(vinagre, "0.020"));
        ingredientes.add(new Ingrediente(vinho, "0.055"));
        ingredientes.add(new Ingrediente(cenoura, "0.020"));
        ingredientes.add(new Ingrediente(cebola, "0.040"));
        ingredientes.add(new Ingrediente(salsa, "0.002"));
        ingredientes.add(new Ingrediente(salvia, "0.0004"));
        ingredientes.add(new Ingrediente(tomilho, "0.0004"));
        ingredientes.add(new Ingrediente(louro, "0.0004"));
        ingredientes.add(new Ingrediente(sal, "0.020"));
        temperoFrango.setIngredientes(ingredientes);
        produtoBC.inserirProduto(temperoFrango);
        
        // Produto Processado
        ProdutoProcessado frangoTemperado = new ProdutoProcessado("Frango Temperado", EnumUnidadeMedida.KG);
        ingredientes = new ArrayList<Ingrediente>();
        ingredientes.add(new Ingrediente(frango, "1"));
        ingredientes.add(new Ingrediente(temperoFrango, "1"));
        frangoTemperado.setIngredientes(ingredientes);
        produtoBC.inserirProduto(frangoTemperado);

        ProdutoProcessado frangoCarcacaTemperado = new ProdutoProcessado("Frango Carcaça Temperado", EnumUnidadeMedida.KG);
        ingredientes = new ArrayList<Ingrediente>();
        ingredientes.add(new Ingrediente(frangoCarcaca, "1"));
        ingredientes.add(new Ingrediente(temperoFrango, "1"));
        frangoCarcacaTemperado.setIngredientes(ingredientes);
        produtoBC.inserirProduto(frangoCarcacaTemperado);
        
        // Produto Finalizado
        ProdutoFinalizado frangoAssado = new ProdutoFinalizado("Frango Assado 2.3kg", EnumUnidadeMedida.UNI);
        ingredientes = new ArrayList<Ingrediente>();
        ingredientes.add(new Ingrediente(frangoTemperado, "2.3"));
        frangoAssado.setIngredientes(ingredientes);
        frangoAssado.setRendimento("1");
        frangoAssado.setPrecoVenda("24");
        frangoAssado.ativar();
        custos = new ArrayList<Custo>();
        custos.add(new Custo(papelAcoplado));
        custos.add(new Custo(sacola));
        custos.add(new Custo(barbante, "0.02"));
        frangoAssado.setDemaisCustos(custos);
        produtoBC.inserirProduto(frangoAssado);

        ProdutoFinalizado frangoAssado2 = new ProdutoFinalizado("Frango Assado 2.5kg", EnumUnidadeMedida.UNI);
        ingredientes = new ArrayList<Ingrediente>();
        ingredientes.add(new Ingrediente(frangoTemperado, "2.5"));
        frangoAssado2.setIngredientes(ingredientes);
        frangoAssado2.setRendimento("1");
        frangoAssado2.setPrecoVenda("24");
//        frangoAssado2.ativar();
        custos = new ArrayList<Custo>();
//        custos.add(new Custo(gas));
        custos.add(new Custo(papelAcoplado));
        custos.add(new Custo(sacola));
        custos.add(new Custo(barbante, "0.02"));
        frangoAssado2.setDemaisCustos(custos);
        produtoBC.inserirProduto(frangoAssado2);

        ProdutoFinalizado frangoAssadoCarcaca = new ProdutoFinalizado("Frango Assado Carcaça", EnumUnidadeMedida.UNI);
        ingredientes = new ArrayList<Ingrediente>();
        ingredientes.add(new Ingrediente(frangoCarcacaTemperado, "1.9"));
        frangoAssadoCarcaca.setIngredientes(ingredientes);
        frangoAssadoCarcaca.setRendimento("1");
        frangoAssadoCarcaca.setPrecoVenda("24");
//        frangoAssadoCarcaca.ativar();
        custos = new ArrayList<Custo>();
//        custos.add(new Custo(gas));
        custos.add(new Custo(papelAcoplado));
        custos.add(new Custo(sacola));
        custos.add(new Custo(barbante, "0.02"));
        frangoAssadoCarcaca.setDemaisCustos(custos);
        produtoBC.inserirProduto(frangoAssadoCarcaca);

        ProdutoFinalizado costelaAssada = new ProdutoFinalizado("Costela Assada", EnumUnidadeMedida.KG);
        ingredientes = new ArrayList<Ingrediente>();
        ingredientes.add(new Ingrediente(costela, "2"));
        ingredientes.add(new Ingrediente(cebola, "0.100"));
        ingredientes.add(new Ingrediente(manjerona, "0.001"));
        ingredientes.add(new Ingrediente(manjericao, "0.001"));
        ingredientes.add(new Ingrediente(salsa, "0.001"));
        ingredientes.add(new Ingrediente(salvia, "0.001"));
        ingredientes.add(new Ingrediente(salGrosso, "0.100"));
        costelaAssada.setIngredientes(ingredientes);
        custos = new ArrayList<Custo>();
        custos.add(new Custo(celofane, "0.030"));
        custos.add(new Custo(carvao, "0.25"));
        custos.add(new Custo(papelAcoplado));
        custos.add(new Custo(sacola));
        costelaAssada.setDemaisCustos(custos);
        costelaAssada.setRendimento("1.164");
        costelaAssada.setPrecoVenda("36");
        costelaAssada.ativar();
        produtoBC.inserirProduto(costelaAssada);
        
        ProdutoFinalizado costelaGaucha = new ProdutoFinalizado("Costela Gaucha", EnumUnidadeMedida.KG);
        ingredientes = new ArrayList<Ingrediente>();
        ingredientes.add(new Ingrediente(costela, "1"));
        ingredientes.add(new Ingrediente(salGrosso, "0.1"));
        costelaGaucha.setIngredientes(ingredientes);
        custos = new ArrayList<Custo>();
        custos.add(new Custo(carvao, "0.4"));
        custos.add(new Custo(papelAcoplado));
        custos.add(new Custo(sacola));
        costelaGaucha.setDemaisCustos(custos);
        costelaGaucha.setRendimento("0.5");
        costelaGaucha.setPrecoVenda("50");
        produtoBC.inserirProduto(costelaGaucha);
//
//        ProdutoFinalizado pernilAssado = new ProdutoFinalizado("Pernil Assado", EnumUnidadeMedida.KG);
//        ingredientes = new ArrayList<Ingrediente>();
//        ingredientes.add(new Ingrediente(pernil, "1"));
//        ingredientes.add(new Ingrediente(limao, "0.1"));
//        ingredientes.add(new Ingrediente(sal, "0.120"));
//        ingredientes.add(new Ingrediente(acucar, "0.030"));
//        pernilAssado.setIngredientes(ingredientes);
//        pernilAssado.setRendimento("0.6");
//        pernilAssado.setPrecoVenda("29");
//        pernilAssado.ativar();
//        custos = new ArrayList<Custo>();
////        custos.add(new Custo(gas));
////        pernilAssado.setDemaisCustos(custos);
//        produtoBC.inserirProduto(pernilAssado);
//
//        ProdutoFinalizado copaLomboAssado = new ProdutoFinalizado("Copa Lombo Assado", EnumUnidadeMedida.KG);
//        ingredientes = new ArrayList<Ingrediente>();
//        ingredientes.add(new Ingrediente(copa, "1"));
//        ingredientes.add(new Ingrediente(limao, "0.1"));
//        ingredientes.add(new Ingrediente(sal, "0.120"));
//        ingredientes.add(new Ingrediente(acucar, "0.030"));
//        copaLomboAssado.setIngredientes(ingredientes);
//        copaLomboAssado.setRendimento("0.650");
//        copaLomboAssado.setPrecoVenda("36");
//        copaLomboAssado.ativar();
//        custos = new ArrayList<Custo>();
////        custos.add(new Custo(gas));
////        copaLomboAssado.setDemaisCustos(custos);
//        produtoBC.inserirProduto(copaLomboAssado);
//
//        ProdutoFinalizado coxaAssada = new ProdutoFinalizado("Coxa com Sobrecoxa Assada", EnumUnidadeMedida.KG);
//        ingredientes = new ArrayList<Ingrediente>();
//        ingredientes.add(new Ingrediente(coxa, "2"));
//        coxaAssada.setIngredientes(ingredientes);
//        coxaAssada.setRendimento("1.02");
//        coxaAssada.setPrecoVenda("21");
//        coxaAssada.ativar();
//        custos = new ArrayList<Custo>();
////        custos.add(new Custo(gas));
////        coxaAssada.setDemaisCustos(custos);
//        produtoBC.inserirProduto(coxaAssada);
//        
//        ProdutoProcessado linguicaAssada = new ProdutoProcessado("Linguiça Assada KG", EnumUnidadeMedida.KG);
//        ingredientes = new ArrayList<Ingrediente>();
//        ingredientes.add(new Ingrediente(linguica, "1"));
//        linguicaAssada.setIngredientes(ingredientes);
//        linguicaAssada.setRendimento("1");
////        custos = new ArrayList<Custo>();
////        custos.add(new Custo(gas));
////        linguicaAssada.setDemaisCustos(custos);
////        linguicaAssada.ativar();
//        produtoBC.inserirProduto(linguicaAssada);
//
//        ProdutoFinalizado linguicaAssadaUnidade = new ProdutoFinalizado("Linguiça Assada", EnumUnidadeMedida.UNI);
//        ingredientes = new ArrayList<Ingrediente>();
//        ingredientes.add(new Ingrediente(linguicaAssada, "1"));
//        linguicaAssadaUnidade.setIngredientes(ingredientes);
//        linguicaAssadaUnidade.setRendimento("12");
//        linguicaAssadaUnidade.setPrecoVenda("2");
//        linguicaAssadaUnidade.ativar();
//        produtoBC.inserirProduto(linguicaAssadaUnidade);
//
//
//        // Produto Processado
//        ProdutoProcessado frangoDesfiado = new ProdutoProcessado("Frango Desfiado", EnumUnidadeMedida.UNI, "1");
//        ingredientes = new ArrayList<Ingrediente>();
//        ingredientes.add(new Ingrediente(frangoAssado, "1"));
//        frangoDesfiado.setIngredientes(ingredientes);
//        produtoBC.inserirProduto(frangoDesfiado);
//
//        // Produto Processado/Finalizado conforme ordem de processamento
//        ProdutoProcessado recheioFrango = new ProdutoProcessado("Recheio para Frango Assado", EnumUnidadeMedida.UNI, "12");
//        ingredientes = new ArrayList<Ingrediente>();
//        ingredientes.add(new Ingrediente(cebola, "0.120"));
//        ingredientes.add(new Ingrediente(sal, "0.020"));
//        ingredientes.add(new Ingrediente(salsa, "0.060"));
//        ingredientes.add(new Ingrediente(azeitona, "0.072"));
//        ingredientes.add(new Ingrediente(moela, "0.300"));
//        ingredientes.add(new Ingrediente(figado, "0.480"));
//        ingredientes.add(new Ingrediente(farinhaMandioca, "0.150"));
//        ingredientes.add(new Ingrediente(farinhaMilhoBiju, "1"));
//        recheioFrango.setIngredientes(ingredientes);
//        recheioFrango.ativar();
//        produtoBC.inserirProduto(recheioFrango);
//
//        ProdutoFinalizado frangoAssadoRecheio = new ProdutoFinalizado("Frango Assado (2.85) Com Recheio", EnumUnidadeMedida.UNI);
//        ingredientes = new ArrayList<Ingrediente>();
//        ingredientes.add(new Ingrediente(frangoAssado, "1"));
//        ingredientes.add(new Ingrediente(recheioFrango, "1"));
//        frangoAssadoRecheio.setIngredientes(ingredientes);
//        frangoAssadoRecheio.setRendimento("1");
//        frangoAssadoRecheio.setPrecoVenda("27");
//        frangoAssadoRecheio.ativar();
//        produtoBC.inserirProduto(frangoAssadoRecheio);
//        ProdutoFinalizado frangoAssadoRecheio1 = new ProdutoFinalizado("Frango Assado (2.5) Com Recheio", EnumUnidadeMedida.UNI);
//        ingredientes = new ArrayList<Ingrediente>();
//        ingredientes.add(new Ingrediente(frangoAssado3, "1"));
//        ingredientes.add(new Ingrediente(recheioFrango, "1"));
//        frangoAssadoRecheio1.setIngredientes(ingredientes);
//        frangoAssadoRecheio1.setRendimento("1");
//        frangoAssadoRecheio1.setPrecoVenda("27");
//        frangoAssadoRecheio1.ativar();
//        produtoBC.inserirProduto(frangoAssadoRecheio1);
//        ProdutoFinalizado frangoAssadoRecheio2 = new ProdutoFinalizado("Frango Assado (carcaça) Com Recheio", EnumUnidadeMedida.UNI);
//        ingredientes = new ArrayList<Ingrediente>();
//        ingredientes.add(new Ingrediente(frangoAssadoCarcaca, "1"));
//        ingredientes.add(new Ingrediente(recheioFrango, "1"));
//        frangoAssadoRecheio2.setIngredientes(ingredientes);
//        frangoAssadoRecheio2.setRendimento("1");
//        frangoAssadoRecheio2.setPrecoVenda("27");
//        frangoAssadoRecheio2.ativar();
//        produtoBC.inserirProduto(frangoAssadoRecheio2);
//
//        ingredientes = new ArrayList<Ingrediente>();
//        ingredientes.add(new Ingrediente(arroz, "1.3"));
//        ingredientes.add(new Ingrediente(frangoDesfiado, "1"));
//        ingredientes.add(new Ingrediente(oleo, "0.1"));
//        ingredientes.add(new Ingrediente(sal, "0.04"));
//        ingredientes.add(new Ingrediente(cebola, "0.2"));
//        ingredientes.add(new Ingrediente(polpa, "1"));
//        ingredientes.add(new Ingrediente(milho, "2"));
//        ingredientes.add(new Ingrediente(cheiroVerde, "2"));
//        ingredientes.add(new Ingrediente(tomate, "0.9"));
//        produtoBC.inserirProduto(new ProdutoFinalizado("Risoto de Frango", EnumUnidadeMedida.UNI, "9", ingredientes, "12", true));
//
//
        MateriaPrima frangoAssadoRisoto = new MateriaPrima("Frango Assado p/ Risoto", EnumUnidadeMedida.UNI, "27", false);
        produtoBC.inserirProduto(frangoAssadoRisoto);
        ProdutoFinalizado risoto = new ProdutoFinalizado("Risoto de Frango PV", EnumUnidadeMedida.UNI);
        ingredientes = new ArrayList<Ingrediente>();
        ingredientes.add(new Ingrediente(arroz, "1.3"));
        ingredientes.add(new Ingrediente(frangoAssadoRisoto, "1"));
        ingredientes.add(new Ingrediente(oleo, "0.1"));
        ingredientes.add(new Ingrediente(sal, "0.04"));
        ingredientes.add(new Ingrediente(cebola, "0.2"));
        ingredientes.add(new Ingrediente(polpa, "1"));
        ingredientes.add(new Ingrediente(milho, "2"));
        ingredientes.add(new Ingrediente(cheiroVerde, "2"));
        ingredientes.add(new Ingrediente(tomate, "0.9"));
        risoto.desativar();
        risoto.setPrecoVenda("12");
        risoto.setRendimento("9");
        risoto.setIngredientes(ingredientes);
        produtoBC.inserirProduto(risoto);
        
//*** MAIONESE
        ingredientes = new ArrayList<Ingrediente>();
        ingredientes.add(new Ingrediente(batata, "6"));
        ingredientes.add(new Ingrediente(oleo, "0.8"));
        ingredientes.add(new Ingrediente(ovo, "6"));
        ingredientes.add(new Ingrediente(sal, "0.05"));
        ingredientes.add(new Ingrediente(leite, "0.2"));
        ingredientes.add(new Ingrediente(milho, "2"));
        ingredientes.add(new Ingrediente(cheiroVerde, "2"));
        ingredientes.add(new Ingrediente(maionese, "0.3"));
        ingredientes.add(new Ingrediente(limao, "0.5"));
        ProdutoFinalizado maioneseBatata = new ProdutoFinalizado("Maionese de Batata", EnumUnidadeMedida.KG);
        maioneseBatata.setPrecoVenda("16");
        maioneseBatata.setRendimento("6.5");
        maioneseBatata.setIngredientes(ingredientes);
        maioneseBatata.ativar();
        produtoBC.inserirProduto(maioneseBatata);
//***
//        
//        ProdutoFinalizado kit = new ProdutoFinalizado("Kit Frango", EnumUnidadeMedida.UNI);
//        ingredientes = new ArrayList<Ingrediente>();
//        ingredientes.add(new Ingrediente(frangoAssadoRecheio, "1"));  
//        ingredientes.add(new Ingrediente(risoto, "1"));  
////        ingredientes.add(new Ingrediente(maioneseBatata, "0.5"));  
//        kit.setIngredientes(ingredientes);
//        kit.setRendimento("1");
//        kit.setPrecoVenda("40");
//        kit.ativar();
//        produtoBC.inserirProduto(kit);
    }
    
    private static void listarProdutos(String... tipos) {
		logger.debug("listando produtos");

		int i = 0;
		for (Produto produto : produtoBC.localizarProdutos(tipos)) {
			if (i == 0) {
				System.out.println("Listando produtos encontrados...");
				i++;
			}
			printProdutoFormatado(produto);
		}
	}

	@SuppressWarnings("unused")
	private static void listarProdutosVendaAtivos() {
		logger.debug("listando produtos ativos a venda");

		int i = 0;
		for (Produto produto : produtoBC.localizarTodosProdutosVenda()) {
			if (produto.getSituacao().equals(EnumSituacaoProduto.ATIVO)) {
				if (i == 0) {
					System.out.println("\nListando produtos ativos a venda encontrados...");
					i++;
				}
				printProdutoFormatado(produto);
			}
		}
	}

	@SuppressWarnings("unused")
	private static void listarProdutosVendaInativos() {
		logger.debug("listando produtos inativos a venda");

		int i = 0;
		for (Produto produto : produtoBC.localizarTodosProdutosVenda()) {
			if (produto.getSituacao().equals(EnumSituacaoProduto.INATIVO)) {
				if (i == 0) {
					System.out.println("\nListando produtos inativos a venda encontrados...");
					i++;
				}
				printProdutoFormatado(produto);
			}
		}
	}

	private static void printProdutoFormatado(Produto produto) {
		if (produto instanceof ProdutoVenda) {
			System.out.println("\t" + produto.getNome() + " - " + produto.getCustoMedioUnitario() + " "
					+ produto.getUnidadeMedida().getSigla() + " - Preço=" + produto.getPrecoVenda() + " ("
					+ produto.getPrecoVenda().subtract(produto.getCustoMedioUnitario()) + ")");
		} else {
			System.out.println("\t" + produto.getNome() + " - " + produto.getCustoMedioUnitario() + " "
					+ produto.getUnidadeMedida().getSigla());
		}
	}


}
