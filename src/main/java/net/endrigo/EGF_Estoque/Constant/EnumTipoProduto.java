package net.endrigo.EGF_Estoque.Constant;

public enum EnumTipoProduto {
	FINAL,
	ACABADO,
	PROCESSADO,
	MATERIA_PRIMA,
	MATERIAL_GERAL;
	
}
