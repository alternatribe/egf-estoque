package net.endrigo.EGF_Estoque.Constant;

public enum EnumUnidadeMedida {
	UNI ("Unidade", "un"),
	KG ("Quilo", "kg"),
	G("Grama", "g"),
	L("Litro", "l"),
	ML("Mililitro", "ml");
	
	private String descrição;
	
	private String sigla;

	private EnumUnidadeMedida(String descrição, String sigla) {
		this.descrição = descrição;
		this.setSigla(sigla);
	}

	public String getDescrição() {
		return descrição;
	}

	public void setDescrição(String descrição) {
		this.descrição = descrição;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	

}
