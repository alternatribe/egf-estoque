package net.endrigo.EGF_Estoque.Domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Ingrediente extends AbstractBean<Long> {

    private static final long serialVersionUID = -4270036160310520423L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ingredienteGenerator")
    @SequenceGenerator(name = "ingredienteGenerator", sequenceName = "INGREDIENTE_SEQ", allocationSize = 1, initialValue = 1)
    private Long id;

    @OneToOne
    @JoinColumn
    private ProdutoAbstrato produto;

    @Column(nullable = false)
    private BigDecimal quantidade;

    public Ingrediente() {}

    public Ingrediente(ProdutoAbstrato produto, String quantidade) {
        this.produto = produto;
        this.quantidade = new BigDecimal(quantidade);
    }

    @Override
    public Long getId() {
        // TODO Auto-generated method stub
        return null;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProdutoAbstrato getProduto() {
        return produto;
    }

    public void setProduto(ProdutoAbstrato produto) {
        this.produto = produto;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(String quantidade) {
        this.quantidade = new BigDecimal(quantidade);
    }

    @Override
    public String toString() {
        return quantidade + produto.getUnidadeMedida().getSigla() + " " + produto.getNome();
    }


}
