package net.endrigo.EGF_Estoque.Domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Fornecedor extends AbstractBean<Long>{
	
	private static final long serialVersionUID = -1828132820585345257L;

	@Id 
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="fornecedorGenerator")
	@SequenceGenerator(name="fornecedorGenerator", sequenceName="FORNECEDOR_SEQ", allocationSize=1, initialValue=1)
	private Long id;
	
	@Column(nullable=false)
	private String nome;
	
	public Fornecedor() {
	}
	
	public Fornecedor(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Fornecedor [id=" + id + ", nome=" + nome + "]";
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fornecedor other = (Fornecedor) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
