package net.endrigo.EGF_Estoque.Domain;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;

import net.endrigo.EGF_Estoque.Constant.EnumSituacaoProduto;
import net.endrigo.EGF_Estoque.Constant.EnumUnidadeMedida;

@Entity
public final class ProdutoFinalizado extends ProdutoProcessado implements ProdutoVenda{

	private static final long serialVersionUID = 6376530043215799548L;

	public ProdutoFinalizado() {
	}
	
	public ProdutoFinalizado(String nome, EnumUnidadeMedida unidadeMedida) {
		super(nome, unidadeMedida);
	}
	
	public ProdutoFinalizado(String nome, EnumUnidadeMedida unidadeMedida, boolean ativo) {
		super(nome, unidadeMedida);
		this.setSituacao(ativo ? EnumSituacaoProduto.ATIVO : EnumSituacaoProduto.INATIVO);
	}

	public ProdutoFinalizado(String nome, EnumUnidadeMedida unidadeMedida, List<Ingrediente> ingredientes, boolean ativo) {
		this.setNome(nome);
		this.setUnidadeMedida(unidadeMedida);
		this.setRendimento("1");
		this.setIngredientes(ingredientes);
		this.setSituacao(ativo ? EnumSituacaoProduto.ATIVO : EnumSituacaoProduto.INATIVO);
	}
	
	public ProdutoFinalizado(String nome, EnumUnidadeMedida unidadeMedida, String rendimento, List<Ingrediente> ingredientes, String preco, boolean ativo) {
		this.setNome(nome);
		this.setUnidadeMedida(unidadeMedida);
		this.setRendimento(rendimento);
		this.setIngredientes(ingredientes);
		this.setPrecoVenda(preco);
		this.setSituacao(ativo ? EnumSituacaoProduto.ATIVO : EnumSituacaoProduto.INATIVO);
	}
	
	@Override
	public BigDecimal getPrecoVenda() {
		if (precoVenda == null) {
			precoVenda = new BigDecimal(0);
		}
		return precoVenda.setScale(2, BigDecimal.ROUND_HALF_UP);
	}

    @Override
    public void setPrecoVenda(String precoVenda) {
        this.precoVenda = new BigDecimal(precoVenda);
    }

}
