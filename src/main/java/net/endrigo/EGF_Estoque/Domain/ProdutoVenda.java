package net.endrigo.EGF_Estoque.Domain;

import java.math.BigDecimal;

public interface ProdutoVenda {

    BigDecimal getPrecoVenda();
    void setPrecoVenda(String precoVenda);
}
