package net.endrigo.EGF_Estoque.Domain;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.persistence.Entity;

import net.endrigo.EGF_Estoque.Constant.EnumSituacaoProduto;
import net.endrigo.EGF_Estoque.Constant.EnumUnidadeMedida;

@Entity
public final class ProdutoFinal extends Produto implements ProdutoCompra, ProdutoVenda{

	private static final long serialVersionUID = -7232394175360357645L;

	public ProdutoFinal() {
	}
	
	public ProdutoFinal(String nome, EnumUnidadeMedida unidadeMedida) {
		super(nome, unidadeMedida);
	}
	
	public ProdutoFinal(String nome, EnumUnidadeMedida unidadeMedida, String custo, String preco, boolean ativo) {
		this.setNome(nome);
		this.setUnidadeMedida(unidadeMedida);
		this.setEstoqueMinimo(0L);
		this.setCustoMedioUnitario(custo);
		this.setPrecoVenda(preco);
		this.setSituacao(ativo ? EnumSituacaoProduto.ATIVO : EnumSituacaoProduto.INATIVO);
		this.getSituacao();
	}
	
    @Override
    public void setCustoMedioUnitario(String custoMedio) {
        this.custoMedioUnitario = new BigDecimal(custoMedio);
    }
    
    @Override
    public void setQuantidadeDisponivel(Long quantidadeDisponivel) {
        this.quantidadeDisponivel = quantidadeDisponivel;
    }
    
    @Override
    public void atualizarProdutoComprado(Long quantidade, String valor) {
        BigDecimal custoTotal = new BigDecimal(0);
        Long novaQuantidade = quantidade;
        if (this.getQuantidadeDisponivel() > 0) {
            custoTotal = getCustoTotal();
            novaQuantidade = this.getQuantidadeDisponivel() + quantidade;
        }
        if (novaQuantidade > 0) {
            custoTotal = custoTotal.add(new BigDecimal(valor).multiply(new BigDecimal(quantidade.longValue())));
            this.setCustoMedioUnitario(custoTotal.divide(new BigDecimal(novaQuantidade), 10, RoundingMode.HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
        } else {
            this.setCustoMedioUnitario(new BigDecimal(valor).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
        }
        this.setQuantidadeDisponivel(novaQuantidade);
    }
    
	@Override
	public BigDecimal getPrecoVenda() {
		if (precoVenda == null) {
			precoVenda = new BigDecimal(0);
		}
		return precoVenda.setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	@Override
	public void setPrecoVenda(String precoVenda) {
		this.precoVenda = new BigDecimal(precoVenda);
	}


	
	
}
