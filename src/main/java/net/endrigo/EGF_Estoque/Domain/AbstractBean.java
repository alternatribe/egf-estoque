package net.endrigo.EGF_Estoque.Domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Version;

@MappedSuperclass
public abstract class AbstractBean<T extends Serializable> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Version
	@Column(name = "VERSION", nullable = false)
	private Timestamp version;
	
	@Column(nullable=false, updatable=false)
	private LocalDateTime dataCriacao;
	
	@Column(nullable=false)
	private LocalDateTime dataModificacao;
	
	
    public AbstractBean(T id, Timestamp version) {
		this.version = version;
	}

	public AbstractBean() {
	}

    public abstract T getId();

	public LocalDateTime getDataCriacao() {
		return dataCriacao;
	}

	public LocalDateTime getDataModificacao() {
		return dataModificacao;
	}

	@PrePersist
    protected void onCreate() {
		dataModificacao = dataCriacao = LocalDateTime.now();
    }

    @PreUpdate
    protected void onUpdate() {
    	dataModificacao = LocalDateTime.now();
    }
    

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof AbstractBean)) {
			return false;
		}
		if (getId() == null || ((AbstractBean<?>) obj).getId() == null) {
			return false;
		}
		if (!getId().equals(((AbstractBean<?>) obj).getId())) {
			return false;
		}
		return true;
	}
	
	@Override
	public int hashCode() {
		return getId() == null ? super.hashCode() : getId().hashCode();
	}
}
