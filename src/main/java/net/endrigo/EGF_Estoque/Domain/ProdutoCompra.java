package net.endrigo.EGF_Estoque.Domain;

public interface ProdutoCompra {

    void setCustoMedioUnitario(String valorUnitario);
    void setQuantidadeDisponivel(Long quantidadeComprada);
    void atualizarProdutoComprado(Long quantidade, String valor);
}
