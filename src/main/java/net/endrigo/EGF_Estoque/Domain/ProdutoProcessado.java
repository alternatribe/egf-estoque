package net.endrigo.EGF_Estoque.Domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import net.endrigo.EGF_Estoque.Constant.EnumUnidadeMedida;

@Entity
public class ProdutoProcessado extends ProdutoAbstrato {

    private static final long serialVersionUID = -7232394175360357645L;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private Collection<Ingrediente> ingredientes = new ArrayList<Ingrediente>();

    private BigDecimal rendimento = new BigDecimal("1");

    public ProdutoProcessado() {
        super();
    }

    public ProdutoProcessado(String nome) {
        super(nome, EnumUnidadeMedida.UNI);
    }
    
    public ProdutoProcessado(String nome, EnumUnidadeMedida unidadeMedida) {
        super(nome, unidadeMedida);
    }

    public ProdutoProcessado(String nome, EnumUnidadeMedida unidadeMedida, String rendimento) {
        super(nome, unidadeMedida);
        this.setRendimento(rendimento);
    }

    public Collection<Ingrediente> getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(List<Ingrediente> ingredientes) {
        this.ingredientes = ingredientes;
    }

    public BigDecimal getRendimento() {
        return rendimento;
    }

    public void setRendimento(String rendimento) {
        this.rendimento = new BigDecimal(rendimento);
    }

    @Override
    public BigDecimal getCustoMedioUnitario() {
        BigDecimal custo = new BigDecimal(0);
        for (Ingrediente ingrediente : this.getIngredientes()) {
            custo = custo.add(ingrediente.getQuantidade().multiply(ingrediente.getProduto().getCustoMedioUnitario()));
        }
        custo = custo.divide(this.getRendimento(), 2, BigDecimal.ROUND_HALF_UP);
        for (Custo demaisCustos : getDemaisCustos()) {
        	custo = custo.add(demaisCustos.getInsumo().getCustoMedioUnitario().multiply(demaisCustos.getQuantidade()));
        }
        return custo.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " [Código=" + getId() + ", Nome=" + getNome() + ", Ingredientes=" + getIngredientes() + ", Insumos=" + getDemaisCustos() + "Custo=" + getCustoMedioUnitario() + " "
                + getUnidadeMedida().getSigla() + "]";
    }

}
