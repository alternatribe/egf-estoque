package net.endrigo.EGF_Estoque.Domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import net.endrigo.EGF_Estoque.Constant.EnumSituacaoProduto;
import net.endrigo.EGF_Estoque.Constant.EnumUnidadeMedida;

/**
 * <b>Produto</b> é uma classe abstrata que representa todo produto, material e serviço utilizado pelo sistema.
 * @author 01730510906
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TIPO")
public abstract class Produto extends AbstractBean<Long> {

    private static final long serialVersionUID = -8902236010420918152L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "produtoGenerator")
    @SequenceGenerator(name = "produtoGenerator", sequenceName = "PRODUTO_SEQ", allocationSize = 1, initialValue = 1)
    private Long id;

    @Column(nullable = false, unique = true, length = 30)
    private String nome;

    // Tipo do produto - usado pelo single_table
    @Column(insertable = false, updatable = false)
    private String tipo;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private EnumSituacaoProduto situacao = EnumSituacaoProduto.INATIVO;

    protected BigDecimal custoMedioUnitario = new BigDecimal(0);

    protected Long quantidadeDisponivel = 0L;

    private Long estoqueMinimo = 0L;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private EnumUnidadeMedida unidadeMedida;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private Collection<Custo> demaisCustos = new ArrayList<Custo>();

    @OneToMany
    @JoinTable(name = "PRODUTO_COMPRADO", joinColumns = @JoinColumn(name = "Produto_ID"), inverseJoinColumns = @JoinColumn(name = "itens_ID"))
    private Collection<ItemCompra> itensComprados;

    protected BigDecimal precoVenda;

    public Produto() {}

    public Produto(String nome, EnumUnidadeMedida unidadeMedida) {
        this.nome = nome;
        this.unidadeMedida = unidadeMedida;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public EnumSituacaoProduto getSituacao() {
        return situacao;
    }
    
    protected void setSituacao(EnumSituacaoProduto situacao) {
        this.situacao = situacao;
    }
    
    public void ativar() {
        this.situacao = EnumSituacaoProduto.ATIVO;
    }

    public void desativar() {
        this.situacao = EnumSituacaoProduto.INATIVO;
    }

    public BigDecimal getCustoMedioUnitario() {
        return custoMedioUnitario.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    /** custoTotal = custoMedioUnitario * quantidadeDisponivel */
    public BigDecimal getCustoTotal() {
        return this.getCustoMedioUnitario().multiply(new BigDecimal(this.getQuantidadeDisponivel()));
    }

    public Long getQuantidadeDisponivel() {
        return quantidadeDisponivel;
    }
    
    public Long getEstoqueMinimo() {
        return estoqueMinimo;
    }

    public void setEstoqueMinimo(Long estoqueMinimo) {
        this.estoqueMinimo = estoqueMinimo;
    }

    public Collection<ItemCompra> getItensComprados() {
        return itensComprados;
    }

    public EnumUnidadeMedida getUnidadeMedida() {
        return unidadeMedida;
    }

    public void setUnidadeMedida(EnumUnidadeMedida unidadeMedida) {
        this.unidadeMedida = unidadeMedida;
    }

    public Collection<Custo> getDemaisCustos() {
        return demaisCustos;
    }

    public void setDemaisCustos(Collection<Custo> demaisCustos) {
        this.demaisCustos = demaisCustos;
    }

    public BigDecimal getPrecoVenda() {
        return new BigDecimal(0);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " [Código=" + getId() + ", Nome=" + getNome() + ", Custo=" + getCustoMedioUnitario() + " " + getUnidadeMedida().getSigla() + ", Qtda=" + getQuantidadeDisponivel() +"]";
    }
}
