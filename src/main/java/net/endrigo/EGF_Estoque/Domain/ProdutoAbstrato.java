package net.endrigo.EGF_Estoque.Domain;

import javax.persistence.Entity;

import net.endrigo.EGF_Estoque.Constant.EnumUnidadeMedida;

@Entity
public abstract class ProdutoAbstrato extends Produto {

	private static final long serialVersionUID = 2951294476354233518L;
	
	public ProdutoAbstrato() {
	}
	
	public ProdutoAbstrato(String nome, EnumUnidadeMedida unidadeMedida) {
		super(nome, unidadeMedida);
	}
}
