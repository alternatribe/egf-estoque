package net.endrigo.EGF_Estoque.Domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity	
public class ItemCompra extends AbstractBean<Long> {

	private static final long serialVersionUID = 63833345319421943L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="itemCompraGenerator")
	@SequenceGenerator(name="itemCompraGenerator", sequenceName="ITEMCOMPRA_SEQ", allocationSize=1, initialValue=1)
	private Long id;
	
	private ProdutoCompra produto;
	
	private Fornecedor fornecedor;
	
	private Long quantidadeComprada = 0L;
	
	private BigDecimal valorUnitario = new BigDecimal(0);
	
	@Column(nullable=false, updatable=false)
	private LocalDateTime dataCompra = LocalDateTime.now();
	
	public ItemCompra() {
	}
	
    public ItemCompra(ProdutoCompra produto, Long quantidadeComprada, String valorUnitario) {
        this.produto = produto;
        this.quantidadeComprada = quantidadeComprada;
        this.setValorUnitario(valorUnitario);
    }
    
    public ItemCompra(LocalDateTime dataCompra, ProdutoCompra produto, Long quantidadeComprada, String valorUnitario) {
        this(produto, quantidadeComprada, valorUnitario);
        this.dataCompra = dataCompra;
	}
	
	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public ProdutoCompra getProduto() {
		return produto;
	}

	public void setProduto(ProdutoCompra produto) {
		this.produto = produto;
	}
	
	public LocalDateTime getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(LocalDateTime dataCompra) {
		this.dataCompra = dataCompra;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

    public Long getQuantidadeComprada() {
		return quantidadeComprada;
	}

	public void setQuantidadeComprada(Long quantidadeComprada) {
		this.quantidadeComprada = quantidadeComprada;
	}

	public BigDecimal getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(String valorUnitario) {
		this.valorUnitario = new BigDecimal(valorUnitario);
	}
}
