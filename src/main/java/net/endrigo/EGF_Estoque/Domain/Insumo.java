package net.endrigo.EGF_Estoque.Domain;

import java.math.BigDecimal;

import javax.persistence.Entity;

import net.endrigo.EGF_Estoque.Constant.EnumSituacaoProduto;
import net.endrigo.EGF_Estoque.Constant.EnumUnidadeMedida;

/**
 * <b>Insumo</b> é todo e qualquer elemento necessário em um processo de produção. 
 * <br>Nesse grupo estão os produtos usados na fabricação, o maquinário, a energia e a própria mão de obra empregada, por exemplo.
 * <b>Matéria-prima é um tipo de insumo, mas aqui não é considerado.</b>
 * <br><br>Podemos considerar que insumos são o conjunto dos fatores de produção que são diretamente combinados para gerar um bem ou serviço. Ou seja, para que um fator de produção seja considerado um insumo, ele precisa ter envolvimento direto com a produção.
 */
@Entity
public final class Insumo extends Produto {

	private static final long serialVersionUID = -3412288527854335687L;

	private String descricao;
	/** Quantidade de itens estimado por unidade de insumo	 */
	private Long quantidadeItensPorInsumo = 1L;
	
	public Insumo() {
	}

	public Insumo(String nome, EnumUnidadeMedida unidadeMedida, String custo, boolean ativo) {
		super(nome, unidadeMedida);
		this.custoMedioUnitario = new BigDecimal(custo);
		this.setSituacao(ativo ? EnumSituacaoProduto.ATIVO : EnumSituacaoProduto.INATIVO);
	}
	
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getQuantidadeItensPorInsumo() {
		return quantidadeItensPorInsumo;
	}

	public void setQuantidadeItensPorInsumo(Long quantidadeItensPorInsumo) {
		this.quantidadeItensPorInsumo = quantidadeItensPorInsumo;
	}

}
