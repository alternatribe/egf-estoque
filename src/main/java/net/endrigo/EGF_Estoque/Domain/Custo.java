package net.endrigo.EGF_Estoque.Domain;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Custo extends AbstractBean<Long> {

	private static final long serialVersionUID = 4530217572161045681L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="custoGenerator")
	@SequenceGenerator(name="custoGenerator", sequenceName="CUSTO_SEQ", allocationSize=1, initialValue=1)
	private Long id;
	
	@OneToOne
	@JoinColumn
	private Insumo insumo;
	
	@Column(nullable=false)
	private BigDecimal quantidade;
	
	public Custo() {
	}
	
	public Custo(Insumo insumo, String quantidade) {
		this.insumo = insumo;
		this.quantidade = new BigDecimal(quantidade);
	}
	
	public Custo(Insumo insumo) {
		this.insumo = insumo;
		BigDecimal qtdaPorInsumo = new BigDecimal(insumo.getQuantidadeItensPorInsumo());
		BigDecimal qtdaEstimada = new BigDecimal("1").divide(qtdaPorInsumo, 2, RoundingMode.HALF_UP);
		this.quantidade = qtdaEstimada;
	}	
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public Insumo getInsumo() {
		return insumo;
	}

	public void setInsumo(Insumo insumo) {
		this.insumo = insumo;
	}

	public BigDecimal getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}
	
    @Override
    public String toString() {
        return quantidade + insumo.getUnidadeMedida().getSigla() + " " + insumo.getNome();
    }

}
