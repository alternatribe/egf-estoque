package net.endrigo.EGF_Estoque;

import static org.junit.Assert.assertTrue;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.junit.Test;

import net.endrigo.EGF_Estoque.Util.JpaUtil;

public class EntityManagerTest {

	@Test
	public void testaConexao() {
		EntityManager em = JpaUtil.getEntityManager();
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		transaction.commit();
		boolean isconectado = em.isOpen();
		em.close();
		assertTrue(isconectado);
	}

}
